package com.devcamp.task56c20.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task56c20.models.Artist;

@Service
public class ArtistService {
    @Autowired
    private AlbumService albumService;

    Artist artist1 = new Artist(96, "vol1");
    Artist artist2 = new Artist(97, "vol2");
    Artist artist3 = new Artist(98, "vol3");

    public ArrayList<Artist> getAllArtist() {
        artist1.setAlbums(albumService.getAllAlbumVol1());
        artist2.setAlbums(albumService.getAllAlbumVol2());
        artist3.setAlbums(albumService.getAllAlbumVol3());

        ArrayList<Artist> artists = new ArrayList<>();
        artists.add(artist1);
        artists.add(artist2);
        artists.add(artist3);
        return artists;
    }

    public Artist fillteArtist(int artistId) {
        ArrayList<Artist> allArtists = getAllArtist();

        Artist artist = new Artist();
        for (Artist artistElement : allArtists) {
            if(artistElement.getId() == artistId ){
                artist = artistElement;
            }
        }
        return artist;
    }
    public Artist artistIndex(int index){
        Artist artist = new Artist();
        if(index > -1 && index < 3){
            artist = getAllArtist().get(index);
        }
        return artist;
    }
}
