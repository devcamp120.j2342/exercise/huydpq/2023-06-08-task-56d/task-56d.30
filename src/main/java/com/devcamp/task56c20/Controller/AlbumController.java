package com.devcamp.task56c20.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56c20.Service.AlbumService;
import com.devcamp.task56c20.models.Album;

@RestController
@CrossOrigin
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    @GetMapping("/album-info")
    public Album getAblumInfo(@RequestParam(required = true, name = "id") int albumId) {
        Album album = albumService.getAlArrayListAlbum(albumId);

        return album;
    }
}
